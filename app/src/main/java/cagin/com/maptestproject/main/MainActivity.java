package cagin.com.maptestproject.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import cagin.com.maptestproject.RecyclerItemClickListener;
import cagin.com.maptestproject.databinding.ActivityMainBinding;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cagin.com.maptestproject.R;
import cagin.com.maptestproject.map.MapsActivity;
import cagin.com.maptestproject.service.model.PoiList;

public class MainActivity extends AppCompatActivity {
    private MainViewModel mainViewModel;
    private ActivityMainBinding mainBinding;
    private List<PoiList> vehicleList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBinding();
        setRecycler(mainBinding.recylerVehicle);
        mainViewModel.sendVehicleRequest();
        mainBinding.ivMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMapActivity(vehicleList);
            }
        });
        mainViewModel.getVehicleResponseObservable().observe(this, new Observer<List<PoiList>>() {
            @Override
            public void onChanged(@Nullable List<PoiList> networkResponse) {
                Log.d("MainActivity","onChanged started");
                VehicleAdapter vehicleAdapter = (VehicleAdapter) mainBinding.recylerVehicle.getAdapter();
                vehicleAdapter.setSongList(networkResponse);
                vehicleList=networkResponse;
                mainBinding.progressBar.setVisibility(View.GONE);
            }
        });
        mainBinding.recylerVehicle.addOnItemTouchListener(
                new RecyclerItemClickListener(this, mainBinding.recylerVehicle ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Log.d("onItemClicked",String.valueOf( vehicleList.get(position).getHeading()));
                        List<PoiList> poiLists = new ArrayList();
                        poiLists.add(vehicleList.get(position));
                        startMapActivity(poiLists);
                    }
                    @Override public void onLongItemClick(View view, int position) {
                    }
                })
        );
    }

    private void initBinding(){
        Log.d("MainActivity","initbinding started");
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.init(this);
        Log.d("MainActivity","initbinding completed");
    }

    private void setRecycler(RecyclerView recyclerVehicle){
        Log.d("MainActivity","setRecycler fired");
        VehicleAdapter songAdapter = new VehicleAdapter();
        recyclerVehicle.setAdapter(songAdapter);
        recyclerVehicle.setLayoutManager(new LinearLayoutManager(this));
        Log.d("MainActivity","setRecycler completed");
    }

    //gson + TypeToken has better performance than serializing pojo class.
    private void startMapActivity(List<PoiList> listPrm){
        Gson gson = new Gson();
        Type type = new TypeToken<List<PoiList>>() {}.getType();
        String json = gson.toJson(listPrm, type);
        Intent intent = new Intent(getBaseContext(), MapsActivity.class);
        intent.putExtra("data", json);
        startActivity(intent);
    }
}
