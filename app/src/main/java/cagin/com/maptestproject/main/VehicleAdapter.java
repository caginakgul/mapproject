package cagin.com.maptestproject.main;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import cagin.com.maptestproject.databinding.RowVehicleBinding;
import cagin.com.maptestproject.R;
import cagin.com.maptestproject.service.model.PoiList;

public class VehicleAdapter  extends RecyclerView.Adapter<VehicleAdapter.VehicleAdapterViewHolder>{

    private List<PoiList> vehicleList;

    public VehicleAdapter() {
        this.vehicleList = Collections.emptyList();
    }

    @Override
    public VehicleAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowVehicleBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.row_vehicle, parent, false);

        VehicleAdapterViewHolder viewHolder = new VehicleAdapterViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VehicleAdapterViewHolder holder, int position) {
        PoiList vehicleDetail = vehicleList.get(position);
        holder.rowVehicleBinding.setVehicleItem(vehicleDetail);

    }

    @Override
    public int getItemCount() {
        if(vehicleList!=null){
            Log.d("getItemCount: ",String.valueOf(vehicleList.size()));
        }
        else{
            Log.d("getItemCount: ","vehicleList is null");
        }
        return vehicleList.size();
    }

    public void setSongList(List<PoiList> vehicleList) {
        this.vehicleList = vehicleList;
        notifyDataSetChanged();
    }

    public static class VehicleAdapterViewHolder extends RecyclerView.ViewHolder {

        public RowVehicleBinding rowVehicleBinding;

        public VehicleAdapterViewHolder(RowVehicleBinding rowVehicleBinding) {
            super(rowVehicleBinding.getRoot());
            this.rowVehicleBinding = rowVehicleBinding;
        }

    }
}
