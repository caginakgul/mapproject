package cagin.com.maptestproject.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import java.util.List;

import cagin.com.maptestproject.service.model.PoiList;
import cagin.com.maptestproject.service.repository.TaxiDetailRepository;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class MainViewModel extends ViewModel {
    private Context context;
    private MutableLiveData<List<PoiList>> vehicleResponseObservable;

    public void init(@NonNull Context context){
        Log.d("MainViewModel","init started");
        this.context=context;
        vehicleResponseObservable = new MutableLiveData<>();

        Log.d("MainViewModel","init completed");
    }

    public void sendVehicleRequest(){
        Log.d("MainViewModel","sendVehicleRequest started");
        TaxiDetailRepository taxiDetailRepository = new TaxiDetailRepository();
        taxiDetailRepository.taxiDetailRequest(context, vehicleResponseObservable);
        Log.d("MainViewModel","sendVehicleRequest finished");
    }

    public LiveData<List<PoiList>> getVehicleResponseObservable() {
        Log.d("MainViewModel", "getContactResponseObservable() " );
        return vehicleResponseObservable;
    }


}
