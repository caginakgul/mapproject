package cagin.com.maptestproject.app;

import android.app.Application;
import android.content.Context;

import cagin.com.maptestproject.service.dagger.AppComponent;
import cagin.com.maptestproject.service.dagger.DaggerAppComponent;
import cagin.com.maptestproject.service.retrofit.RetrofitService;

public class AppController extends Application {
    private static AppComponent appComponent;

    private static AppController get(Context context){
        return (AppController) context.getApplicationContext();
    }

    public static AppController create(Context context) {
        return AppController.get(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildAppComponent();
    }

    public AppComponent buildAppComponent() {
        return DaggerAppComponent.builder().retrofitService(new RetrofitService()).build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
