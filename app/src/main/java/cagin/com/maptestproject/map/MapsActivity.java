package cagin.com.maptestproject.map;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import cagin.com.maptestproject.R;
import cagin.com.maptestproject.service.model.PoiList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<PoiList> vehicleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        readData();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d("MapsActivity","onMapReady");
        LatLng vehicle;
        if(vehicleList.size()==1){ // for animating camera
            //if user display sinlge vehicle
           vehicle = new LatLng(vehicleList.get(0).getCoordinate().getLatitude(), vehicleList.get(0).getCoordinate().getLongitude());
        }
        else{
            //if user displays all available vehicles  - center of hamburg
            vehicle = new LatLng(53.548300, 9.989999);
        }
        for(PoiList poi : vehicleList){
            Log.d("LatLong Poi", String.valueOf(poi.getCoordinate().getLatitude()));
            LatLng vehicleCoordinate = new LatLng(poi.getCoordinate().getLatitude(),poi.getCoordinate().getLongitude());
            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_black)).position(vehicleCoordinate).title(String.valueOf(poi.getId())));
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(vehicle,10));
    }


    private void readData(){
        Log.d("MapsActivity","readData");
        Gson gson = new Gson();
        String stringLocation = getIntent().getStringExtra("data");
        if(stringLocation != null) {
            Type type = new TypeToken<List<PoiList>>() {
            }.getType();
            vehicleList = gson.fromJson(stringLocation, type);
            Log.d("Location Count", Integer.toString(vehicleList.size()));
            }
        else{
            Log.d("Location Count","failed");
        }
        Log.d("MapsActivity","readData finished");
    }

}
