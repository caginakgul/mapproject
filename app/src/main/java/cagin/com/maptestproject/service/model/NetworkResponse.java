package cagin.com.maptestproject.service.model;

import java.util.List;

public class NetworkResponse {
    private List<PoiList> poiList;

    public List<PoiList> getPoiList() {
        return poiList;
    }

    public void setPoiList(List<PoiList> poiList) {
        this.poiList = poiList;
    }
}
