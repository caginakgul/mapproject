package cagin.com.maptestproject.service.model;

public class PoiList {
    private int id;
    private Coordinate coordinate;
    private String fleetType;
    private Double heading;

    public PoiList(int id, Coordinate coordinate, String fleetType, Double heading) {
        this.id = id;
        this.coordinate = coordinate;
        this.fleetType = fleetType;
        this.heading = heading;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public String getFleetType() {
        return fleetType;
    }

    public void setFleetType(String fleetType) {
        this.fleetType = fleetType;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }
}
