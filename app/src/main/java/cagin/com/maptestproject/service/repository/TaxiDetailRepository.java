package cagin.com.maptestproject.service.repository;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import cagin.com.maptestproject.app.AppController;
import cagin.com.maptestproject.service.model.NetworkResponse;
import cagin.com.maptestproject.service.model.PoiList;
import cagin.com.maptestproject.service.retrofit.IService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaxiDetailRepository {
    @Inject
    IService service;

    AppController appController;

    public void taxiDetailRequest(Context context, final MutableLiveData<List<PoiList>> data) {
        appController = AppController.create(context);
        appController.getAppComponent().inject(this);

        service.getTaxiList().enqueue(new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                Log.d("onFailure ", "success!");
                data.postValue(response.body().getPoiList());
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                Log.d("onFailure ", "error!");
                data.setValue(null);
            }
        });
    }
}
