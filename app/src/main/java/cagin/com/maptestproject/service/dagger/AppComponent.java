package cagin.com.maptestproject.service.dagger;

import javax.inject.Singleton;

import cagin.com.maptestproject.service.repository.TaxiDetailRepository;
import cagin.com.maptestproject.service.retrofit.RetrofitService;
import dagger.Component;

@Component(modules = {RetrofitService.class})
@Singleton
public interface AppComponent {
    void inject(TaxiDetailRepository taxiDetailRepository);
}
