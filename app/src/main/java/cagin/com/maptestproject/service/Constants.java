package cagin.com.maptestproject.service;

public class Constants {
    public static final class Api{
        public static final String BASE_URL = "https://fake-poi-api.mytaxi.com/";
        public static final String TAXI_LIST = "?p1Lat=53.694865&p1Lon=9.757589&p2Lat=53.394655&p2Lon=10.099891";
    }
}
