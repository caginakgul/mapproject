package cagin.com.maptestproject.service.retrofit;

import cagin.com.maptestproject.service.Constants;
import cagin.com.maptestproject.service.model.NetworkResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface IService {
    @GET(Constants.Api.TAXI_LIST)
    Call<NetworkResponse> getTaxiList();
}
